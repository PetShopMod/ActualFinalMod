
package queuetester_animalshelter;

///////////////////////////////////////////////////////////////////////////////
//                   
// Title:                         Animal Shelter
// Semester:                   COP3804 –Fall 2017
//
// Author:      5917227
// Instructor’ Name: Prof. Charters
// Description   Helper class for Animal Shelter    
// 
//
//////////////////////////// 80 columns wide/////////////////////////////////

public class QueueHelper
{
    public Queue animalQueue = new Queue();
    public Queue dogQueue = new Queue();
    public Queue catQueue = new Queue();
    public void enqueueDogs(Pet dog)
    {
        dogQueue.enqueue(dog);
        enqueueAnimals(dog);
    }
    
    public void enqueueCats(Pet cat)
    {
        catQueue.enqueue(cat);
        enqueueAnimals(cat);
    }
    
    public void enqueueAnimals(Pet animal)
    {
        animalQueue.enqueue(animal);
    }
    
    public Pet dequeueDogs()
    {
       Pet dog = (Pet) dogQueue.dequeue();
       Pet animal = (Pet) animalQueue.dequeue();
       return dog;
       
    }
    
    public Pet dequeueCats()
    {
        Pet cat = (Pet) catQueue.dequeue();
        Pet animal = (Pet) animalQueue.dequeue();
        return cat;
    }
    
    public Pet dequeueAnimals()
    {
        Pet animal = (Pet) animalQueue.dequeue();
        return animal;
    }
    
}
