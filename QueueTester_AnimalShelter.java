///////////////////////////////////////////////////////////////////////////////
//                   
// Title:                         Animal Shelter
// Semester:                   COP3804 –Fall 2017
//
// Author:      5917227
// Instructor’ Name: Prof. Charters
// Description :  Program  that simulates an animal shelter by letting you donate and adopt 
//  dogs, cats, or the oldest animal in the shelter
//
//////////////////////////// 80 columns wide/////////////////////////////////
package queuetester_animalshelter;

/**
 *
 * @author Juan
 */
import javax.swing.JOptionPane;
public class QueueTester_AnimalShelter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int choice = 0;
        Pet newPet;
        QueueHelper QH = new QueueHelper();
        do 
        {
        String stringChoice =  JOptionPane.showInputDialog(null, "Please choose an option 1-6"
                + "\n 1. Donate a Dog "
                + "\n 2. Donate a Cat "
                + "\n 3. Adopt a Dog "
                + "\n 4. Adopt a Cat "
                + "\n 5. Adopt Oldest Animal "
                + "\n 6. Exit" );
        
        choice = Integer.parseInt(stringChoice);
        
         switch (choice) 
            {      
            case 1:
                {
                    String dog = JOptionPane.showInputDialog(null, "What is the name of the dog?");
                    newPet = new Pet(dog, "dog");
                    QH.enqueueDogs(newPet);
                }
                break;
            
            case 2:
                {
                    String cat = JOptionPane.showInputDialog(null, "What is the name of the cat?");
                    newPet = new Pet(cat, "cat");
                    QH.enqueueCats(newPet);
                }
                break;
            case 3:
                {
                    newPet = QH.dequeueDogs();
                    JOptionPane.showMessageDialog(null, "Congratulations on Adopting " + newPet + "!");
                }
                break;
            case 4:
                {
                    newPet = QH.dequeueCats();
                    JOptionPane.showMessageDialog(null, "Congratulations on Adopting " + newPet + "!");
                }
                break;
            case 5:
                {
                    newPet = QH.dequeueAnimals();
                    JOptionPane.showMessageDialog(null, "Congratulations on Adopting " + newPet + "!");                
                }
                break;
            case 6:
                {
                    JOptionPane.showMessageDialog(null, "Bye bye!");
                }
            break;
            default:
                {
                   stringChoice = JOptionPane.showInputDialog(null, "Not a valid option, please choose an option #1-6"
                           + "\n 1.Donate a Dog"
                           + "\n 2.Donate a Cat"
                           + "\n 3.Adopt a Dog"
                           + "\n 4.Adopt a Cat"
                           + "\n 5.Adopt Oldest Animal"
                           + "\n 6.Exit");
                   choice = Integer.parseInt(stringChoice);
                }
                break;
         } 
        }
        while (choice != 6);
        // TODO code application logic here
    }
    
}
