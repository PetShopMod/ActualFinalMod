///////////////////////////////////////////////////////////////////////////////
//                   
// Title:                         Animal Shelter
// Semester:                   COP3804 –Fall 2017
//
// Author:      5917227
// Instructor’ Name: Prof. Charters
// Description   Defines the Queue class used in the program 
// 
//
//////////////////////////// 80 columns wide/////////////////////////////////
package queuetester_animalshelter;

/**
 *
 * @author Juan
 */
import java.util.NoSuchElementException;
public class Queue {
    
   private Node first;
   private Node last;
   
   public Queue()
   {
       first = null;
       last = null;
   }
   
   public void enqueue(Object obj)
   {
      Node newNode = new Node();
      newNode.data = obj;
      newNode.next = null;
      if (last != null)
      {
          last.next = newNode;
      }
      
      if(first == null)
      {
          first = newNode;
      }
      
      last = newNode;
   }
   
   public Object dequeue()
   {
       if (first == null)
       {
           throw new NoSuchElementException();
       }
       Object obj = first.data;
       if (first == last)
       {
           last = null;
       }
       first = first.next;
       return obj;
   }
   
   public boolean isEmpty()
   {
       boolean empty = false;
       if(first == null)
       {   
           
           empty = true;
       }
       return empty;
   }
   
   class Node
   {
       public Object data;
       public Node next;
   }
    
}
